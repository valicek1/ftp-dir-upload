UNIT MyThreads;

{$mode objfpc}{$H+}

INTERFACE

USES
   Classes,
   ftpsend,
   laz_synapse,
   SysUtils;

TYPE
   TFTPSettings = record
     Protocol, Param: string;
     TargetHost: string;
     TargetPort: string;
     TargetFileName: string;
     LocalFileName: string;
     UserName: string;
     Password: string;
     done: integer;
   end;

   TMyThread = CLASS(TThread )
   private
   protected
      PROCEDURE Execute; override;
   public
//      tFTP : TFTPSettings;
      PROPERTY Terminated;
      CONSTRUCTOR Create (CreateSuspended   : Boolean );
   END;

IMPLEMENTATION


CONSTRUCTOR TMyThread.Create (CreateSuspended   : Boolean );
BEGIN
   FreeOnTerminate := True;
   INHERITED Create (CreateSuspended );
END;


PROCEDURE TMyThread.Execute;
BEGIN
//   tFTP.done := 0;
   //(const IP, Port, FileName, LocalFile, User, Pass: string): Boolean;
//   IF FtpPutFile(tFTP.TargetHost, tFTP.TargetPort, tFTP.TargetFileName, tFTP.LocalFileName, tFTP.UserName, tFTP.Password) THEN
//     tFTP.done := 1
//   ELSE
//     tFTP.done := -1;
END;

END.

