PROGRAM FtpDirUpload;

{$mode objfpc}{$H+}

USES {$IFDEF UNIX} {$IFDEF UseCThreads}
   cthreads, {$ENDIF} {$ENDIF}
   Classes,
   SysUtils,
   laz_synapse,
   CustApp,
   ftpsend,
   FileUtil,
   crt,
   md5,
   IniFiles;

TYPE

   TFTPSettings = RECORD
      Protocol, Param  : String;
      TargetHost  : String;
      TargetPort  : String;
      TargetFileName  : String;
      UserName  : String;
      Password  : String;
      done  :     Integer;
   END;

   { TFtpDirUpload }

   TFtpDirUpload = CLASS(TCustomApplication )
   protected
      PROCEDURE DoRun; override;
   public
   END;


   FUNCTION LocFileSize (FileName   : String )  : Int64;
   BEGIN
      Result := FileSize (FileName );
   END;


   FUNCTION SeparateLeft (CONST Value, Delimiter   : String )  : String;
   VAR
      x  : Integer;
   BEGIN
      x := Pos (Delimiter, Value );
      IF x < 1 THEN
      BEGIN
         Result := Value;
      END
      ELSE
      BEGIN
         Result := Copy (Value, 1, x - 1 );
      END;
   END;


   FUNCTION SeparateRight (CONST Value, Delimiter   : String )  : String;
   VAR
      x  : Integer;
   BEGIN
      x := Pos (Delimiter, Value );
      IF x > 0 THEN
      BEGIN
         x := x + Length (Delimiter ) - 1;
      END;
      Result := Copy (Value, x + 1, Length (Value ) - x );
   END;


   FUNCTION ParseURL (URL   : String; VAR Prot, User, Pass, Host, Port, Path, Para   : String )  : String;
   VAR
      x, y  :   Integer;
      sURL  :   String;
      s  :      String;
      s1, s2  : String;
   BEGIN
      Prot := 'http';
      User := '';
      Pass := '';
      Port := '80';
      Para := '';

      x := Pos ('://', URL );
      IF x > 0 THEN
      BEGIN
         Prot := SeparateLeft (URL, '://' );
         sURL := SeparateRight (URL, '://' );
      END
      ELSE
      BEGIN
         sURL := URL;
      END;
      IF UpperCase (Prot ) = 'HTTPS' THEN
      BEGIN
         Port := '443';
      END;
      IF UpperCase (Prot ) = 'FTP' THEN
      BEGIN
         Port := '21';
      END;
      x := Pos ('@', sURL );
      y := Pos ('/', sURL );
      IF (x > 0 ) and ((x < y ) or (y < 1 ) ) THEN
      BEGIN
         s    := SeparateLeft (sURL, '@' );
         sURL := SeparateRight (sURL, '@' );
         x    := Pos (':', s );
         IF x > 0 THEN
         BEGIN
            User := SeparateLeft (s, ':' );
            Pass := SeparateRight (s, ':' );
         END
         ELSE
         BEGIN
            User := s;
         END;
      END;
      x := Pos ('/', sURL );
      IF x > 0 THEN
      BEGIN
         s1 := SeparateLeft (sURL, '/' );
         s2 := SeparateRight (sURL, '/' );
      END
      ELSE
      BEGIN
         s1 := sURL;
         s2 := '';
      END;
      IF Pos ('[', s1 ) = 1 THEN
      BEGIN
         Host := Separateleft (s1, ']' );
         Delete (Host, 1, 1 );
         s1 := SeparateRight (s1, ']' );
         IF Pos (':', s1 ) = 1 THEN
         BEGIN
            Port := SeparateRight (s1, ':' );
         END;
      END
      ELSE
      BEGIN
         x := Pos (':', s1 );
         IF x > 0 THEN
         BEGIN
            Host := SeparateLeft (s1, ':' );
            Port := SeparateRight (s1, ':' );
         END
         ELSE
         BEGIN
            Host := s1;
         END;
      END;
      Result := '/' + s2;
      x      := Pos ('?', s2 );
      IF x > 0 THEN
      BEGIN
         Path := '/' + SeparateLeft (s2, '?' );
         Para := SeparateRight (s2, '?' );
      END
      ELSE
      BEGIN
         Path := '/' + s2;
      END;
      IF Host = '' THEN
      BEGIN
         Host := 'localhost';
      END;
   END;


   PROCEDURE FindAll (CONST Path   : String; Attr   : Integer; List   : TStrings );
   VAR
      Res  :     TSearchRec;
      EOFound  : Boolean;
   BEGIN
      EOFound := False;
      IF FindFirst (Path, Attr, Res ) < 0 THEN
      BEGIN
         exit;
      END
      ELSE
      BEGIN
         WHILE not EOFound DO
         BEGIN
            IF (Res.Name <> '.' ) and (Res.Name <> '..' ) THEN
            BEGIN
               IF ExtractFileExt (Res.Name ) <> '' THEN
               BEGIN
                  List.Add (SysToUTF8 (Res.Name ) );
               END;
            END;
            EOFound := FindNext (Res ) <> 0;
         END;
      END;
      FindClose (Res );
   END;

   //hlavní procedura
   PROCEDURE TFtpDirUpload.DoRun;
   VAR
      Files  :  TStringList;
      RFiles  : TStringList;
      I  :      Integer;
      Slozka  : String;
      FTP  :    TFTPSend;
      URL  :    String;
      P  :      TFTPSettings;
      FSLoc, FSRem  : Int64;
      INI  :    TIniFile;
      RHash  :  String; //remote hash
      LHash  :  String; //local hash
   BEGIN
      Writeln ('FTP UPLOAD DIR' );
      Writeln ('==============' );
      Writeln;
      //Pokud nemám parametry
      IF ParamCount < 2 THEN
      BEGIN
         Writeln ('Parametry:' );
         Writeln;
         Writeln ('FtpDirUpload ftp://user:pass@server:port/cesta/k/souborum C:\lokalni\slozka' );
         Writeln ('FtpDirUpload ftp://user:pass@server:port *' );
         Halt (400 );
      END;
      //Paramety ->
      Slozka := Params[2];
      URL    := Params[1];
      //seznam souborů
      Write ('Vytvarim seznam souboru... ' );
      //pokud je tu *, tak vyberu celou složku
      IF slozka = '*' THEN
      BEGIN
         slozka := Location + '*.*';
      END;
      Writeln (slozka );
      //Seznam souborů
      Files := TStringList.Create;
      FindAll (Slozka, faAnyFile, Files );
      //statistika
      Writeln ('Clekem: ', files.Count );
      IF files.Count < 1 THEN
      BEGIN
         //Konec, pokud nejsou soubory
         Writeln ('Zadne soubory, konec....' );
         Halt (400 );
      END;

      //Aplikace MD5 filtru
      Writeln ('Nacitam ini soubor...' );
      INI := TIniFile.Create (Location + 'config.ini' );
      Writeln (Location + 'config.ini' );
      //Vytvorim RFiles
      RFiles := TStringList.Create;
      //Složka, kde jsou soubory
      Slozka := Params[2];
      Slozka := ExtractFilePath (Slozka );
      IF slozka = '' THEN
      BEGIN
         slozka := ExtractFilePath (Location );
      END;
      //pro každej soubor
      FOR i := 0 TO files.Count - 1 DO
      BEGIN
         //načtu si jeho hash z INI
         RHash := INI.ReadString ('hashes', Files[i], '' ); //hash na serveru
         LHash := MD5Print (MD5File (Slozka + Files[i] ) );  //hash doma
         IF LHash <> RHash THEN //pokud se hashe liší, pak přidám do seznamu souborů k přenesení
         BEGIN
            RFiles.Add (Files[i] ); //přidám
            Writeln ('Q: ', Files[i] );
         END;
      END;
      INI.Free;
      Files.Free;

      //vypíšu statistiku
      Writeln ('Pocet po filtru: ', RFiles.Count );

      //Pokud nic neprošlo filtrem
      IF RFiles.Count < 1 THEN
      BEGIN
         //Konec, pokud nejsou soubory
         Writeln ('Zadne soubory neprosly filtrem, konec....' );
         Halt (400 );
      END;

      Writeln;
      //Připravím url serveru
      Writeln ('Parsuji URL...' );
      IF URL = '' THEN
      BEGIN
         Writeln ('Url nezadana!' );
         Halt (400 );
      END;

      //parsování URL
      ParseURL (URL, P.Protocol, P.UserName, P.Password, P.TargetHost, P.TargetPort, P.TargetFileName, {%H-}P.Param );
      Writeln ('Vysledek:' );
      Writeln ('Host:     ', P.Protocol, '://', P.TargetHost, ':', P.TargetPort );
      Writeln ('Uzivatel: ', P.UserName );
      Write ('Heslo:    ' );
      //doplním *
      FOR I := 0 TO Length (P.Password ) - 1 DO
      BEGIN
         Write ('*' );
      END;
      Writeln;
      //cesta k souboru na uložišti
      Writeln ('Cesta:    ', P.TargetFileName );
      Writeln;

      //Přihlášení na FTP
      FTP := TFTPSend.Create;
      WITH FTP DO
      BEGIN
         Username   := P.UserName;
         Password   := P.Password;
         TargetHost := P.TargetHost;
         TargetPort := P.TargetPort;

         Writeln ('Login...' );
         //pokud jsem se přihlásil
         IF Login THEN
         BEGIN
            Writeln ('Prihlaseno!' );
            ChangeToRootDir; //změním na kořenový adresář
            IF not ChangeWorkingDir (P.TargetFileName ) THEN
            BEGIN
               //pokud cesta neexistuje, tak ji vytvorim
               Writeln ('Slozka na vzdalenem serveru neexistuje!' );
               CreateDir (P.TargetFileName );
               WriteLn ('Vytvoreno...' );
            END;
            //prejdu do vytvorene slozky
            Writeln ('Prepinam se do slozky: ', P.TargetFileName );
            ChangeWorkingDir (P.TargetFileName );
            P.Param := P.TargetFileName;
            //pro každý soubor, co je potřeba nahrát
            Writeln ('Uploading...' );
            FOR I := 0 TO RFiles.Count - 1 DO
            BEGIN
               Write('Soubor ', I+1:3, '/', RFiles.Count:3, ': ', RFiles[I], '... ' );
               //Spáchám cestu
               Slozka := Params[2];
               Slozka := ExtractFilePath (Slozka );
               IF slozka = '' THEN
               BEGIN
                  slozka := ExtractFilePath (Location );
               END;
               //přidám soubor
               Slozka     := Slozka + RFiles[I];
               DirectFile := True;
               DirectFileName := Slozka;
               if StoreFile (RFiles[I], False ) then Writeln('Ok...') else Writeln('Error!');
            END;
            //odhlásím se ze serveru
            Logout;
            Writeln ('Odhlaseno!' );
         END
         ELSE
         BEGIN
            Writeln ('Login selhal!' );
         END;
      END;
      Terminate; //konec, aby se to neopakovalo
   END;

VAR
   Application  : TFtpDirUpload;
BEGIN
   Application := TFtpDirUpload.Create (nil );
   Application.Title := 'FTPDirUpload';
   Application.Run;
   Application.Free;
END.
